from .database import Database


class Client:

    def __init__(self):
        self.__db = Database('agronews')

    def checkIsActive(self, contact):
        """
        Verifica se o contato está cadastrado no nosso banco de dados.
        :param contact: Numero do contato
        :return: Retorna as info do contato.
        """
        res = self.__db.select(
            numero=contact,
            column='*'
        )
        if res:
            return {
                'nome': res[1],
                'recado': res[2],
                'numero': res[3],
                'step': res[4]
            }

    def newClient(self, nome, numero, recado):
        """
        Adiciona o contato ao nosso banco de dados.
        :param nome:
        :param numero: Numero do contato
        :param recado:
        :return:
        """
        return self.__db.insert_client(
            nome=nome,
            numero=numero,
            recado=recado
        )

    def changeStep(self, numero, step):
        return self.__db.update(
            numero=numero,
            column='step',
            value=step
        )

    def changeKeyword(self, numero, value):
        return self.__db.update(
            numero=numero,
            column='keyword',
            value=value
        )

    def changeMenu(self, numero, menu):
        return self.__db.update(
            numero=numero,
            column='menu',
            value=menu
        )

    def searchKeyword(self, numero):
        res = self.__db.select(
            numero=numero,
            column='keyword'
        )
        return {
            'keyword': res[0]
        }

    def currentMenu(self, numero):
        res = self.__db.select(
            numero=numero,
            column='menu'
        )
        return {
            'interesse': res[0]
        }
